from functions import *

    

def main():
    turtle.speed(0)
    turtle.penup()
    import_picture("creepy-scary.gif")
    user_name = input("Welcome! What is your name? ")
    time.sleep(1)
    print("Hello, {}! You find yourself in a dark and haunted forest.".format(user_name))
    time.sleep(1)
    print("Make choices to see where the story takes you.")

    paths = [
        {"description": "Enter the old, decrepit mansion and unveil its secrets.", "ending": "Congratulations! You've completed the game."},
        {"description": "Follow the eerie whispers and encounter a ghostly figure.", "ending": "Congratulations! You've completed the game."},
        {"description": "Investigate the mysterious graveyard and face the undead.", "ending": "Congratulations! You've completed the game."},
        {"description": "Escape through the twisted maze and avoid the lurking horrors.", "ending": "Congratulations! You've completed the game."}
    ]

    

    print("\nYou find four paths ahead.")
    time.sleep(1)
    print("Do you:")
    

    initial_choice = make_choice(["Enter the Mansion", "Follow the Whispers", "Investigate the Graveyard", "Escape through the Maze"])

    if initial_choice == 1:
        print("\nYou chose to Enter the Mansion.")
        time.sleep(1)
        display_text("You step inside the creaking mansion with flickering candlelight.")
        move()
        time.sleep(1)
        horror_event()
        print("Do you:")
        time.sleep(1)
        move()
        path_choice = make_choice(["Explore the mansion's dark corridors", "Search for hidden passages"])
        display_text("You chose to {}. {}".format(paths[path_choice-1]['description'], paths[path_choice-1]['ending']))

    elif initial_choice == 2:
        print("\nYou chose to Follow the Whispers.")
        time.sleep(1)
        display_text("The whispers lead you to a ghostly figure shrouded in mist.")
        move()
        time.sleep(1)
        horror_event()
        print("Do you:")
        time.sleep(1)
        move()
        path_choice = make_choice(["Converse with the ghostly figure", "Attempt to flee from the apparition"])
        display_text("You chose to {}. {}".format(paths[path_choice-1]['description'], paths[path_choice-1]['ending']))
        turtle.shape("creepy-scary.gif")
    elif initial_choice == 3:
        print("\nYou chose to Investigate the Graveyard.")
        time.sleep(1)
        display_text("The graveyard is filled with eerie silence and tombstones.")
        move()
        time.sleep(1)
        horror_event()
        print("Do you:")
        time.sleep(1)
        move()
        path_choice = make_choice(["Face the rising undead", "Search for an escape route"])
        display_text("You chose to {}. {}".format(paths[path_choice]['description'], paths[path_choice]['ending']))

    elif initial_choice == 4:
        print("\nYou chose to Escape through the Maze.")
        time.sleep(1)
        display_text("The maze is labyrinthine and unsettling.")
        move()
        time.sleep(1)
        horror_event()
        print("Do you:")
        time.sleep(1)
        move()
        path_choice = make_choice(["Navigate cautiously through the maze", "Run blindly in hopes of finding an exit"])
        display_text("You chose to {}. {}".format(paths[path_choice]['description'], paths[path_choice]['ending']))
        time.sleep(6)
    turtle.goto(0,225)
    
    turtle.shape("creepy-scary.gif")
    
    
    
    
if __name__ == "__main__":
    main()
    
turtle.done()
import time
import turtle
import sys

def make_choice(options):
    print("\nChoose your path:")
    for i, option in enumerate(options, start=1):
        print("{}. {}".format(i, option))

    choice_input = input("Enter the number of your choice (or type 'quit' to exit): ")

    if choice_input.lower() == 'quit':
        turtle.done()
        sys.exit()
    
    try:
        choice = int(choice_input)
        return choice
    except ValueError:
        print("Invalid input. Please enter a number or 'quit'.")
        return make_choice(options)

def display_text(text):
    
    turtle.write(text, align="center", font=("Arial", 16, "normal"))
    time.sleep(1)
    
def import_picture(image_path):
    turtle.register_shape(image_path)

    
def move():
    turtle.right(90)
    turtle.forward(30)
    turtle.left(90)

def horror_event():
    
    turtle.write("A cold wind howls, and shadows dance in the moonlight.", align="center", font=("Arial", 16, "normal"))
    time.sleep(1)
